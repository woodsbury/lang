#include <stdio.h>

#include <platform/compiler.h>

#include <script/lexer.h>

#include <lextable.h>

static inline struct token create_token(struct lexer * l,
                                        char const * start,
                                        enum token_type type);

static inline struct token create_regex_token(struct lexer * l,
                                              char const * start);
static inline struct token create_string_token(struct lexer * l,
                                               char const * start);

static inline struct token create_multi_line_comment(struct lexer *l,
                                                     char const * start);
static inline struct token create_single_line_comment(struct lexer * l,
                                                      char const * start);

static inline void skip_whitespace(struct lexer * l);

struct lexer lexstr(char const * str) {
	struct lexer l;
	l.start = str;
	l.position = str;
	l.line = 1;

	l.paren_depth = 0;
	l.expect_block = 0;
	l.expect_value = 1;

	if (str[0] == '#' && str[1] == '!') {
		// Skip the interpreter directive
		char const * position = str;
		while (*position != '\n' && *position != '\0') {
			++position;
		}

		if (*position == '\n') {
			++position;
			l.line = 2;
		}

		l.position = position;
	}

	return l;
}

struct token lex(struct lexer * l) {
	skip_whitespace(l);

	char const * start = l->position;
	char const * position = start;

	if (unlikely(*position == '\0')) {
		return create_token(l, position, T_END);
	}

	if (*position == '\'' || *position == '"') {
		return create_string_token(l, position);
	}

	if (*position == '/') {
		char nextchar = *(position + 1);

		if (nextchar == '/') {
			return create_single_line_comment(l, position);
		} else if (nextchar == '*') {
			return create_multi_line_comment(l, position);
		} else if (l->expect_value) {
			return create_regex_token(l, position);
		}
	}

	if (out_of_range(*position)) {
		l->position = position + 1;
		return create_token(l, start, T_ERROR_UNEXPECTED);
	}

	int state = 0;

	while (lextable[state][idx(*position)] > -1) {
		state = lextable[state][idx(*position)];
		++position;

		if (out_of_range(*position)) {
			break;
		}
	}

	if (unlikely(state == 0)) {
		l->position = position + 1;
	} else {
		l->position = position;
	}

	return create_token(l, start, lexmap[state]);
}

int lexerror(struct lexer const * restrict l, struct token const * restrict t,
             char const * restrict filename) {
	if (t->type >= 0) {
		return 0;
	}

	switch (t->type) {
	case T_ERROR_UNEXPECTED:
		lexmsg(l, t, "Unexpected character", filename);
		break;

	case T_ERROR_UNTERM_COMMENT:
		lexmsg(l, t, "Unterminated multiline comment", filename);
		break;

	case T_ERROR_UNTERM_REGEX:
		lexmsg(l, t, "Unterminated regular expression literal",
		       filename);
		break;

	case T_ERROR_UNTERM_STRING:
		lexmsg(l, t, "Unterminated string literal", filename);
		break;

	case T_ERROR_UNMATCH_CLOSE_PAREN:
		lexmsg(l, t, "Unmatched close parenthesis", filename);
		break;

	case T_ERROR_UNMATCH_OPEN_PAREN:
		lexmsg(l, t, "Unmatched open parenthesis", filename);
		break;

	case T_ERROR_PAREN_DEPTH:
		lexmsg(l, t, "Parentheses nested too deeply", filename);
		break;

	default:
		lexmsg(l, t, "Unknown error", filename);
	}

	return 1;
}

void lexmsg(struct lexer const * restrict l, struct token const * restrict t,
            char const * restrict msg, char const * restrict filename) {
	char const * line_start = t->start;
	char const * line_end = t->start;

	while (*line_start != '\n' && line_start >= l->start) {
		--line_start;
	}

	while (*line_end != '\n' && *line_end != '\0') {
		++line_end;
	}

	++line_start;
	--line_end;

	int ch = t->start - line_start + 1;

	if (filename) {
		fprintf(stderr, "%s:%u:%d: ", filename, t->line, ch);
	} else {
		fprintf(stderr, "%u:%d: ", t->line, ch);
	}

	fprintf(stderr, "%s\n", msg);

	char const * i = line_start;
	while (i <= line_end) {
		putc(*(i++), stderr);
	}

	putc('\n', stderr);

	for (int i = 0; i < ch - 1; ++i) {
		if (line_start[i] == '\t') {
			putc('\t', stderr);
		} else {
			putc(' ', stderr);
		}
	}

	fprintf(stderr, "^\n");
}

static inline struct token create_token(struct lexer * l,
                                        char const * start,
                                        enum token_type type) {
	struct token t;
	t.start = start;
	t.end = l->position;
	t.line = l->line;
	t.type = type;

	switch (type) {
	case T_INTEGER:
	case T_NULL:
	case T_NUMBER:
	case T_REGEX:
	case T_STRING:
	case T_IDENTIFIER:
	case T_TRUE:
	case T_FALSE:
		if (l->expect_block && l->paren_depth == 0) {
			l->expect_block = 0;
			l->expect_value = 1;
		} else {
			l->expect_value = 0;
		}

		break;

	case T_BREAK:
	case T_CASE:
	case T_CATCH:
	case T_CLASS:
	case T_CONTINUE:
	case T_DEFAULT:
	case T_DO:
	case T_ENUM:
	case T_FINALLY:
	case T_FUNCTION:
	case T_IMPORT:
	case T_NAMESPACE:
	case T_NEW:
	case T_RETURN:
	case T_THIS:
	case T_THROW:
	case T_TRY:
	case T_TYPEOF:
	case T_VAR:
		l->expect_value = 1;
		break;

	case T_ELSE:
	case T_IF:
	case T_FOR:
	case T_SWITCH:
	case T_WHILE:
		l->expect_block = 1;
		l->expect_value = 1;
		break;

	case T_ADD:
	case T_ADD_ASSIGN:
	case T_AND:
	case T_ASSIGN:
	case T_BIT_AND:
	case T_BIT_AND_ASSIGN:
	case T_BIT_NOT:
	case T_BIT_OR:
	case T_BIT_OR_ASSIGN:
	case T_BIT_XOR:
	case T_BIT_XOR_ASSIGN:
	case T_CLOSE_BRACE:
	case T_CLOSE_SQUARE_BRACE:
	case T_COLON:
	case T_COMMA:
	case T_DIVIDE:
	case T_DIVIDE_ASSIGN:
	case T_EQUAL:
	case T_GREATER:
	case T_GREATER_OR_EQUAL:
	case T_IDENTICAL:
	case T_LEFT_SHIFT:
	case T_LEFT_SHIFT_ASSIGN:
	case T_LESS:
	case T_LESS_OR_EQUAL:
	case T_MODULUS:
	case T_MODULUS_ASSIGN:
	case T_MULTIPLY:
	case T_MULTIPLY_ASSIGN:
	case T_NOT:
	case T_NOT_EQUAL:
	case T_NOT_IDENTICAL:
	case T_OPEN_BRACE:
	case T_OPEN_SQUARE_BRACE:
	case T_OR:
	case T_PERIOD:
	case T_RIGHT_SHIFT:
	case T_RIGHT_SHIFT_ASSIGN:
	case T_RIGHT_SHIFT_ZERO:
	case T_RIGHT_SHIFT_ZERO_ASSIGN:
	case T_SEMICOLON:
	case T_SHORT_IF:
	case T_SUBTRACT_ASSIGN:
		l->expect_value = 1;
		break;

	case T_CLOSE_PAREN:
		if (l->paren_depth > 1) {
			l->paren_depth -= 1;
		} else if (l->paren_depth == 1) {
			if (l->expect_block) {
				l->expect_value = 1;
			} else {
				l->expect_value = 0;
			}

			l->paren_depth = 0;
			l->expect_block = 0;
		} else {
			// Unexpected close parenthesis
			t.type = T_ERROR_UNMATCH_CLOSE_PAREN;
		}

		break;

	case T_OPEN_PAREN:
	{
		unsigned int paren_depth = l->paren_depth;
		l->paren_depth += 1;

		if (l->paren_depth < paren_depth) {
			// Overflowed the parenthesis counter
			l->paren_depth = paren_depth;
			t.type = T_ERROR_PAREN_DEPTH;
		}

		break;
	}

	case T_POST_DECREMENT:
		if (l->expect_value) {
			t.type = T_PRE_DECREMENT;
		}

		break;

	case T_POST_INCREMENT:
		if (l->expect_value) {
			t.type = T_PRE_INCREMENT;
		}

		break;

	case T_SUBTRACT:
		if (l->expect_value) {
			t.type = T_NEGATIVE;
		} else {
			l->expect_value = 1;
		}

		break;

	case T_END:
		if (l->paren_depth > 0) {
			l->paren_depth = 0;
			t.type = T_ERROR_UNMATCH_OPEN_PAREN;
		}

	default:
		break;
	}

	return t;
}

static inline struct token create_regex_token(struct lexer * l,
                                              char const * start) {
	char const * position = start + 1;

	while (*position != '/') {
		if (*position == '\0') {
			l->position = position;
			return create_token(l, start, T_ERROR_UNTERM_REGEX);
		}

		++position;
	}

	// Check for modifiers
	++position;

	while ((*position >= 'A' && *position <= 'Z') ||
	       (*position >= 'a' && *position <= 'z')) {
		++position;
	}

	l->position = position;
	return create_token(l, start, T_REGEX);
}

static inline struct token create_string_token(struct lexer * l,
                                               char const * start) {
	char const * position = start + 1;
	char quote = *start;

	while (*position != quote) {
		if (*position == '\\') {
			++position;
		}

		if (*position == '\0') {
			l->position = position;
			return create_token(l, start, T_ERROR_UNTERM_STRING);
		}

		++position;
	}

	l->position = position + 1;
	return create_token(l, start, T_STRING);
}

static inline struct token create_multi_line_comment(struct lexer * l,
                                                     const char * start) {
	char const * position = start + 2;
	unsigned int lines = 0;

	enum token_type type = T_COMMENT;

	while (*position == '/') {
		++position;
	}

	while (*position != '/') {
		while (*position != '*') {
			if (*position == '\0') {
				type = T_ERROR_UNTERM_COMMENT;
				--position;
				goto return_token;
			}

			if (*position == '\n') {
				++lines;
			}

			++position;
		}

		++position;
	}

return_token:
	l->position = position + 1;
	struct token t = create_token(l, start, type);

	l->line += lines;

	return t;
}

static inline struct token create_single_line_comment(struct lexer * l,
                                                      char const * start) {
	char const * position = start + 2;

	while (*position != '\n' && *position != '\0') {
		++position;
	}

	l->position = *position == '\0' ? position : position + 1;
	struct token t = create_token(l, start, T_COMMENT);

	if (*position == '\n') {
		l->line += 1;
	}

	return t;
}

static inline void skip_whitespace(struct lexer * l) {
	char const * position = l->position;

	while (*position == '\n' ||
	       *position == '\t' ||
	       *position == ' ') {
		if (*position == '\n') {
			l->line += 1;
		}

		++position;
	}

	l->position = position;
}
