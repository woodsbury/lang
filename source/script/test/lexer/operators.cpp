#include <test/unit.hpp>

#include <script/lexer.h>

TEST(script, lexer, operators, add) {
	struct lexer l = lexstr("a + b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_ADD);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, add_assign) {
	struct lexer l = lexstr("a += b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_ADD_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, and) {
	struct lexer l = lexstr("a && b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_AND);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, assign) {
	struct lexer l = lexstr("a = b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, bit_and) {
	struct lexer l = lexstr("a & b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_BIT_AND);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, bit_and_assign) {
	struct lexer l = lexstr("a &= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_BIT_AND_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, bit_not) {
	struct lexer l = lexstr("~a");

	struct token t = lex(&l);
	EQUAL(t.type, T_BIT_NOT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, bit_or) {
	struct lexer l = lexstr("a | b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_BIT_OR);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, bit_or_assign) {
	struct lexer l = lexstr("a |= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_BIT_OR_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, bit_xor) {
	struct lexer l = lexstr("a ^ b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_BIT_XOR);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, bit_xor_assign) {
	struct lexer l = lexstr("a ^= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_BIT_XOR_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, braces) {
	struct lexer l = lexstr("{ a }");

	struct token t = lex(&l);
	EQUAL(t.type, T_OPEN_BRACE);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_CLOSE_BRACE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, parens) {
	struct lexer l = lexstr("( a )");

	struct token t = lex(&l);
	EQUAL(t.type, T_OPEN_PAREN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_CLOSE_PAREN);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, close_square_brace) {
	struct lexer l = lexstr("[a]");

	struct token t = lex(&l);
	EQUAL(t.type, T_OPEN_SQUARE_BRACE);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_CLOSE_SQUARE_BRACE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, colon) {
	struct lexer l = lexstr(":");

	struct token t = lex(&l);
	EQUAL(t.type, T_COLON);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, comma) {
	struct lexer l = lexstr(",");

	struct token t = lex(&l);
	EQUAL(t.type, T_COMMA);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, divide) {
	struct lexer l = lexstr("a / b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, divide_assign) {
	struct lexer l = lexstr("a /= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, equal) {
	struct lexer l = lexstr("a == b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_EQUAL);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, greater) {
	struct lexer l = lexstr("a > b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_GREATER);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, greater_or_equal) {
	struct lexer l = lexstr("a >= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_GREATER_OR_EQUAL);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, identical) {
	struct lexer l = lexstr("a === b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_IDENTICAL);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, left_shift) {
	struct lexer l = lexstr("a << b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_LEFT_SHIFT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, left_shift_assign) {
	struct lexer l = lexstr("a <<= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_LEFT_SHIFT_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, less) {
	struct lexer l = lexstr("a < b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_LESS);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, less_or_equal) {
	struct lexer l = lexstr("a <= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_LESS_OR_EQUAL);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, modulus) {
	struct lexer l = lexstr("a % b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_MODULUS);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, modulus_assign) {
	struct lexer l = lexstr("a %= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_MODULUS_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, multiply) {
	struct lexer l = lexstr("a * b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_MULTIPLY);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, multiply_assign) {
	struct lexer l = lexstr("a *= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_MULTIPLY_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, negative) {
	struct lexer l = lexstr("-a");

	struct token t = lex(&l);
	EQUAL(t.type, T_NEGATIVE);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, not) {
	struct lexer l = lexstr("!a");

	struct token t = lex(&l);
	EQUAL(t.type, T_NOT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, not_equal) {
	struct lexer l = lexstr("a != b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_NOT_EQUAL);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, not_identical) {
	struct lexer l = lexstr("a !== b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_NOT_IDENTICAL);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, or) {
	struct lexer l = lexstr("a || b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_OR);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, period) {
	struct lexer l = lexstr(".");

	struct token t = lex(&l);
	EQUAL(t.type, T_PERIOD);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, post_decrement) {
	struct lexer l = lexstr("a--");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_POST_DECREMENT);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, post_increment) {
	struct lexer l = lexstr("a++");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_POST_INCREMENT);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, pre_increment) {
	struct lexer l = lexstr("++a");

	struct token t = lex(&l);
	EQUAL(t.type, T_PRE_INCREMENT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, pre_decrement) {
	struct lexer l = lexstr("--a");

	struct token t = lex(&l);
	EQUAL(t.type, T_PRE_DECREMENT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, right_shift) {
	struct lexer l = lexstr("a >> b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_RIGHT_SHIFT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, right_shift_assign) {
	struct lexer l = lexstr("a >>= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_RIGHT_SHIFT_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, right_shift_zero) {
	struct lexer l = lexstr("a >>> b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_RIGHT_SHIFT_ZERO);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, right_shift_zero_assign) {
	struct lexer l = lexstr("a >>>= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_RIGHT_SHIFT_ZERO_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, semicolon) {
	struct lexer l = lexstr(";");

	struct token t = lex(&l);
	EQUAL(t.type, T_SEMICOLON);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, short_if) {
	struct lexer l = lexstr("?");

	struct token t = lex(&l);
	EQUAL(t.type, T_SHORT_IF);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, subtract) {
	struct lexer l = lexstr("a - b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_SUBTRACT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, operators, subtract_assign) {
	struct lexer l = lexstr("a -= b");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_SUBTRACT_ASSIGN);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}
