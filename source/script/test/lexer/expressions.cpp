#include <test/benchmark.hpp>
#include <test/unit.hpp>

#include <script/lexer.h>

TEST(script, lexer, expressions, comment, multi_line) {
	struct lexer l = lexstr("/*/ Abc * Abc / */ a");

	struct token t = lex(&l);
	EQUAL(t.type, T_COMMENT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, comment, multi_line_line_number) {
	struct lexer l = lexstr("/* Abc\n"
	                        "Abc\n"
	                        "Abc*/");

	struct token t = lex(&l);
	EQUAL(t.type, T_COMMENT);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_END);
	EQUAL(t.line, 3);
}

TEST(script, lexer, expressions, comment, unterminated_multi_line) {
	struct lexer l = lexstr("/* Abc *");

	struct token t = lex(&l);
	EQUAL(t.type, T_ERROR_UNTERM_COMMENT);
}

TEST(script, lexer, expressions, comment, single_line) {
	struct lexer l = lexstr("// Abc\n");

	struct token t = lex(&l);
	EQUAL(t.type, T_COMMENT);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_END);
	EQUAL(t.line, 2);
}

TEST(script, lexer, expressions, comment, empty_single_line) {
	struct lexer l = lexstr("//");

	struct token t = lex(&l);
	EQUAL(t.type, T_COMMENT);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_END);
	EQUAL(t.line, 1);
}

TEST(script, lexer, expressions, number, f_indicator) {
	struct lexer l = lexstr("123f 123.4fe45");

	struct token t = lex(&l);
	EQUAL(t.type, T_INTEGER);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_NUMBER);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_END);
	EQUAL(t.line, 1);
}

TEST(script, lexer, expressions, regex, if_with_brace) {
	struct lexer l = lexstr("if (a) {\n"
	                        "\t/abc/ab\n"
	                        "}\n");

	struct token t = lex(&l);
	EQUAL(t.type, T_IF);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_OPEN_PAREN);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_CLOSE_PAREN);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_OPEN_BRACE);
	EQUAL(t.line, 1);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	EQUAL(t.line, 2);
	t = lex(&l);
	EQUAL(t.type, T_CLOSE_BRACE);
	EQUAL(t.line, 3);
}

TEST(script, lexer, expressions, regex, if_without_brace) {
	struct lexer l = lexstr("if a /abc/ab\n");

	struct token t = lex(&l);
	EQUAL(t.type, T_IF);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, regex, pre_increment) {
	struct lexer l = lexstr("++a / /abc/");

	struct token t = lex(&l);
	EQUAL(t.type, T_PRE_INCREMENT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, regex, post_increment) {
	struct lexer l = lexstr("a++ / /abc/");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_POST_INCREMENT);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, regex, pre_decrement) {
	struct lexer l = lexstr("--a / /abc/");

	struct token t = lex(&l);
	EQUAL(t.type, T_PRE_DECREMENT);
	t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, regex, post_decrement) {
	struct lexer l = lexstr("a-- / /abc/");

	struct token t = lex(&l);
	EQUAL(t.type, T_IDENTIFIER);
	t = lex(&l);
	EQUAL(t.type, T_POST_DECREMENT);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, regex, true) {
	struct lexer l = lexstr("true / /abc/");

	struct token t = lex(&l);
	EQUAL(t.type, T_TRUE);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, regex, false) {
	struct lexer l = lexstr("false / /abc/");

	struct token t = lex(&l);
	EQUAL(t.type, T_FALSE);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, regex, null) {
	struct lexer l = lexstr("null / /abc/");

	struct token t = lex(&l);
	EQUAL(t.type, T_NULL);
	t = lex(&l);
	EQUAL(t.type, T_DIVIDE);
	t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, expressions, benchmark) {
	BENCHMARK {
		struct lexer l = lexstr("#!./parser\n"
		                        "/* A comment */\n"
		                        "var a = 'abc';\n"
		                        "var b = 123;\n"
		                        "var c = 12.3;\n"
		                        "for (var i = 0; i < 10; ++i) {\n"
		                        "\tbreak;\n"
		                        "}\n\n"
		                        "if (a) {\n"
		                        "\tswitch (b) {\n"
		                        "\tcase 1:\n"
		                        "\t\tbreak;\n"
		                        "\tcase 2:\n"
		                        "\t\tbreak;\n"
		                        "}\n\n"
		                        "do {\n"
		                        "\tcontinue;\n"
		                        "} while (0)\n\n"
		                        "return a + b + c;\n");

		struct token t = lex(&l);
		while (t.type != T_END) {
			t = lex(&l);
		}
	}
}
