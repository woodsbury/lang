#include <test/unit.hpp>

#include <script/lexer.h>

TEST(script, lexer, keywords, break) {
	struct lexer l = lexstr("break");

	struct token t = lex(&l);
	EQUAL(t.type, T_BREAK);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, case) {
	struct lexer l = lexstr("case");

	struct token t = lex(&l);
	EQUAL(t.type, T_CASE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, catch) {
	struct lexer l = lexstr("catch");

	struct token t = lex(&l);
	EQUAL(t.type, T_CATCH);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, class) {
	struct lexer l = lexstr("class");

	struct token t = lex(&l);
	EQUAL(t.type, T_CLASS);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, continue) {
	struct lexer l = lexstr("continue");

	struct token t = lex(&l);
	EQUAL(t.type, T_CONTINUE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, default) {
	struct lexer l = lexstr("default");

	struct token t = lex(&l);
	EQUAL(t.type, T_DEFAULT);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, do) {
	struct lexer l = lexstr("do");

	struct token t = lex(&l);
	EQUAL(t.type, T_DO);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, else) {
	struct lexer l = lexstr("else");

	struct token t = lex(&l);
	EQUAL(t.type, T_ELSE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, enum) {
	struct lexer l = lexstr("enum");

	struct token t = lex(&l);
	EQUAL(t.type, T_ENUM);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, false) {
	struct lexer l = lexstr("false");

	struct token t = lex(&l);
	EQUAL(t.type, T_FALSE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, finally) {
	struct lexer l = lexstr("finally");

	struct token t = lex(&l);
	EQUAL(t.type, T_FINALLY);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, for) {
	struct lexer l = lexstr("for");

	struct token t = lex(&l);
	EQUAL(t.type, T_FOR);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, function) {
	struct lexer l = lexstr("function");

	struct token t = lex(&l);
	EQUAL(t.type, T_FUNCTION);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, if) {
	struct lexer l = lexstr("if");

	struct token t = lex(&l);
	EQUAL(t.type, T_IF);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, import) {
	struct lexer l = lexstr("import");

	struct token t = lex(&l);
	EQUAL(t.type, T_IMPORT);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, namespace) {
	struct lexer l = lexstr("namespace");

	struct token t = lex(&l);
	EQUAL(t.type, T_NAMESPACE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, new) {
	struct lexer l = lexstr("new");

	struct token t = lex(&l);
	EQUAL(t.type, T_NEW);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, return) {
	struct lexer l = lexstr("return");

	struct token t = lex(&l);
	EQUAL(t.type, T_RETURN);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, switch) {
	struct lexer l = lexstr("switch");

	struct token t = lex(&l);
	EQUAL(t.type, T_SWITCH);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, this) {
	struct lexer l = lexstr("this");

	struct token t = lex(&l);
	EQUAL(t.type, T_THIS);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, throw) {
	struct lexer l = lexstr("throw");

	struct token t = lex(&l);
	EQUAL(t.type, T_THROW);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, true) {
	struct lexer l = lexstr("true");

	struct token t = lex(&l);
	EQUAL(t.type, T_TRUE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, try) {
	struct lexer l = lexstr("try");

	struct token t = lex(&l);
	EQUAL(t.type, T_TRY);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, typeof) {
	struct lexer l = lexstr("typeof");

	struct token t = lex(&l);
	EQUAL(t.type, T_TYPEOF);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, var) {
	struct lexer l = lexstr("var");

	struct token t = lex(&l);
	EQUAL(t.type, T_VAR);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, keywords, while) {
	struct lexer l = lexstr("while");

	struct token t = lex(&l);
	EQUAL(t.type, T_WHILE);
	t = lex(&l);
	EQUAL(t.type, T_END);
}
