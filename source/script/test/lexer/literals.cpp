#include <test/unit.hpp>

#include <script/lexer.h>

TEST(script, lexer, literals, decimal) {
	struct lexer l = lexstr("12345.67890");

	struct token t = lex(&l);
	EQUAL(t.type, T_DECIMAL);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, decimal_with_exp) {
	struct lexer l = lexstr("12345.67890e10");

	struct token t = lex(&l);
	EQUAL(t.type, T_DECIMAL);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, decimal_without_fraction) {
	struct lexer l = lexstr("12345e10");

	struct token t = lex(&l);
	EQUAL(t.type, T_DECIMAL);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, integer) {
	struct lexer l = lexstr("1234567890");

	struct token t = lex(&l);
	EQUAL(t.type, T_INTEGER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, number) {
	struct lexer l = lexstr("12345.67890f");

	struct token t = lex(&l);
	EQUAL(t.type, T_NUMBER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, number_with_exp) {
	struct lexer l = lexstr("12345.67890e10f");

	struct token t = lex(&l);
	EQUAL(t.type, T_NUMBER);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, regex, no_mods) {
	struct lexer l = lexstr("/abc/");

	struct token t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, regex, mods) {
	struct lexer l = lexstr("/abc/ab");

	struct token t = lex(&l);
	EQUAL(t.type, T_REGEX);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, regex, unterminated) {
	struct lexer l = lexstr("/abc");

	struct token t = lex(&l);
	EQUAL(t.type, T_ERROR_UNTERM_REGEX);
}

TEST(script, lexer, literals, string, single) {
	struct lexer l = lexstr("'abc'");

	struct token t = lex(&l);
	EQUAL(t.type, T_STRING);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, string, double) {
	struct lexer l = lexstr("\"abc\"");

	struct token t = lex(&l);
	EQUAL(t.type, T_STRING);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, string, escape_single) {
	struct lexer l = lexstr("'abc\\'abc'");

	struct token t = lex(&l);
	EQUAL(t.type, T_STRING);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, string, escape_double) {
	struct lexer l = lexstr("\"abc\\\"abc\"");

	struct token t = lex(&l);
	EQUAL(t.type, T_STRING);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, string, escape_slash) {
	struct lexer l = lexstr("'abc\\\\'");

	struct token t = lex(&l);
	EQUAL(t.type, T_STRING);
	t = lex(&l);
	EQUAL(t.type, T_END);
}

TEST(script, lexer, literals, string, unterminated) {
	struct lexer l = lexstr("'abc");

	struct token t = lex(&l);
	EQUAL(t.type, T_ERROR_UNTERM_STRING);
}

TEST(script, lexer, literals, strings, unterminated_escape) {
	struct lexer l = lexstr("'abc\\");

	struct token t = lex(&l);
	EQUAL(t.type, T_ERROR_UNTERM_STRING);
}
