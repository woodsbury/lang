#include <stddef.h>
#include <stdlib.h>

#include <variant/type.h>
#include <variant/value.h>
#include <variant/types/base.h>
#include <variant/types/function.h>
#include <variant/types/integer.h>
#include <variant/types/number.h>

variant * varcreate(struct variant_type * type, size_t nargs,
                    variant * args[]) {
	variant * v;

	if (type == NULL) {
		v = malloc(sizeof(struct variant_base));
		v->type = NULL;
	} else {
		v = malloc(type->size);
		v->type = type;

		if (type->ctor && type->ctor(v, nargs, args)) {
			free(v);
			return NULL;
		}
	}

	return v;
}

int vardestroy(variant * v) {
	if (v->type && v->type->dtor && v->type->dtor(v)) {
		free(v);
		return 1;
	}

	free(v);
	return 0;
}

struct variant_type * vartype(variant const * v) {
	return v == NULL ? NULL : v->type;
}

char const * vartypename(struct variant_type * type) {
	return type == NULL ? NULL : type->name;
}

variant * varfromfloat(double val) {
	struct number_variant * v = malloc(sizeof(struct number_variant));
	v->base.type = varnumtype();
	v->value = val;
	return (variant *) v;
}

double vartofloat(variant const * v) {
	if (vartype(v) == varnumtype()) {
		return ((struct number_variant *) v)->value;
	}

	return 0.0;
}

variant * varfromfunc(variant_function func) {
	struct function_variant * v = malloc(sizeof(struct function_variant));
	v->base.type = varfunctype();
	v->function = func;
	return (variant *) v;
}

variant_function vartofunc(variant const * v) {
	if (vartype(v) == varfunctype()) {
		return ((struct function_variant *) v)->function;
	}

	return NULL;
}

variant * varfromint(int64_t val) {
	struct integer_variant * v = malloc(sizeof(struct integer_variant));
	v->base.type = varinttype();
	v->value = val;
	return (variant *) v;
}

int64_t vartoint(variant const * v) {
	if (vartype(v) == varinttype()) {
		return ((struct integer_variant *) v)->value;
	}

	return 0;
}
