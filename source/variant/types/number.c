#include <assert.h>
#include <stddef.h>

#include <variant/type.h>
#include <variant/types/number.h>

static int number_ctor(variant * v, size_t nargs, variant * args[]);

struct variant_type number_variant_type = {
	"Number",
	sizeof(struct number_variant),
	NULL,
	NULL,
	&number_ctor,
	NULL
};

extern inline struct variant_type * varnumtype(void);

static int number_ctor(variant * v, size_t nargs, variant * args[]) {
	assert(vartype(v) == varnumtype());
	struct number_variant * nv = (struct number_variant *) v;

	if (nargs == 0) {
		nv->value = 0.0;
	} else {
		nv->value = vartofloat(args[0]);
	}

	return 0;
}
