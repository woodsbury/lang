#include <assert.h>
#include <stddef.h>

#include <variant/type.h>
#include <variant/types/function.h>

static int function_ctor(variant * v, size_t nargs, variant * args[]);

struct variant_type function_variant_type = {
	"Function",
	sizeof(struct function_variant),
	NULL,
	NULL,
	&function_ctor,
	NULL
};

extern inline struct variant_type * varfunctype(void);

static int function_ctor(variant * v, size_t nargs, variant * args[]) {
	assert(vartype(v) == varfunctype());
	struct function_variant * fv = (struct function_variant *) v;

	if (nargs == 0) {
		return 1;
	} else if (vartype(args[0]) != varfunctype()) {
		return 1;
	}

	fv->function = vartofunc(args[0]);
	return 0;
}
