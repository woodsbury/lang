#include <assert.h>
#include <stddef.h>

#include <variant/type.h>
#include <variant/types/integer.h>

static int integer_ctor(variant * v, size_t nargs, variant * args[]);

struct variant_type integer_variant_type = {
	"Integer",
	sizeof(struct integer_variant),
	NULL,
	NULL,
	&integer_ctor,
	NULL
};

extern inline struct variant_type * varinttype(void);

static int integer_ctor(variant * v, size_t nargs, variant * args[]) {
	assert(vartype(v) == varinttype());
	struct integer_variant * iv = (struct integer_variant *) v;

	if (nargs == 0) {
		iv->value = 0;
	} else {
		iv->value = vartoint(args[0]);
	}

	return 0;
}
