#include <test/unit.hpp>

#include <variant/value.h>
#include <variant/types/number.h>

TEST(variant, number, type) {
	variant * v = varfromfloat(1.1);
	NOT_EQUAL(v, NULL);

	EQUAL(vartype(v), varnumtype());

	EQUAL(vardestroy(v), 0);
}

TEST(variant, number, value) {
	variant * v = varfromfloat(2.2);
	NOT_EQUAL(v, NULL);

	double val = vartofloat(v);
	FLT_EQUAL(val, 2.2);

	EQUAL(vardestroy(v), 0);
}

TEST(variant, number, typename) {
	variant * v = varfromfloat(3.3);
	NOT_EQUAL(v, NULL);

	struct variant_type * type = vartype(v);
	NOT_EQUAL(type, NULL);
	STR_EQUAL(vartypename(type), "Number");

	EQUAL(vardestroy(v), 0);
}

TEST(variant, number, ctor) {
	variant * args[1];
	args[0] = varfromfloat(4.4);
	NOT_EQUAL(args[0], NULL);

	variant * v = varcreate(varnumtype(), 1, args);
	NOT_EQUAL(v, NULL);

	double val1 = vartofloat(args[0]);
	double val2 = vartofloat(v);
	FLT_EQUAL(val1, val2);

	EQUAL(vardestroy(args[0]), 0);
	EQUAL(vardestroy(v), 0);
}

TEST(variant, number, static) {
	struct number_variant v = varfromfloat_static(5.5);

	double val = vartofloat((variant *) &v);
	FLT_EQUAL(val, 5.5);
}
