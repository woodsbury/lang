#include <test/unit.hpp>

#include <variant/value.h>
#include <variant/types/integer.h>

TEST(variant, integer, type) {
	variant * v = varfromint(1);
	NOT_EQUAL(v, NULL);

	EQUAL(vartype(v), varinttype());

	EQUAL(vardestroy(v), 0);
}

TEST(variant, integer, value) {
	variant * v = varfromint(2);
	NOT_EQUAL(v, NULL);

	long long int val = vartoint(v);
	EQUAL(val, 2);

	EQUAL(vardestroy(v), 0);
}

TEST(variant, integer, typename) {
	variant * v = varfromint(3);
	NOT_EQUAL(v, NULL);

	struct variant_type * type = vartype(v);
	NOT_EQUAL(type, NULL);
	STR_EQUAL(vartypename(type), "Integer");

	EQUAL(vardestroy(v), 0);
}

TEST(variant, integer, ctor) {
	variant * args[1];
	args[0] = varfromint(4);
	NOT_EQUAL(args[0], NULL);

	variant * v = varcreate(varinttype(), 1, args);
	NOT_EQUAL(v, NULL);

	long long int val1 = vartoint(args[0]);
	long long int val2 = vartoint(v);
	EQUAL(val1, val2);

	EQUAL(vardestroy(args[0]), 0);
	EQUAL(vardestroy(v), 0);
}

TEST(variant, integer, static) {
	struct integer_variant v = varfromint_static(5);

	long long int val = vartoint((variant *) &v);
	EQUAL(val, 5);
}
