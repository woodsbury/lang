#include <test/unit.hpp>

#include <variant/value.h>
#include <variant/types/function.h>

variant * test_function(variant * self, size_t nargs, variant * args[]) {
	return varfromint(1);
}

TEST(variant, function, type) {
	variant * v = varfromfunc(&test_function);
	NOT_EQUAL(v, NULL);

	EQUAL(vartype(v), varfunctype());

	EQUAL(vardestroy(v), 0);
}

TEST(variant, function, value) {
	variant * v = varfromfunc(&test_function);
	NOT_EQUAL(v, NULL);

	variant_function func = vartofunc(v);
	NOT_EQUAL(func, NULL);

	variant * result = func(NULL, 0, NULL);
	NOT_EQUAL(result, NULL);

	long long int int_result = vartoint(result);
	EQUAL(int_result, 1);

	EQUAL(vardestroy(v), 0);
	EQUAL(vardestroy(result), 0);
}

TEST(variant, function, typename) {
	variant * v = varfromfunc(&test_function);
	NOT_EQUAL(v, NULL);

	struct variant_type * type = vartype(v);
	NOT_EQUAL(type, NULL);
	STR_EQUAL(vartypename(type), "Function");

	EQUAL(vardestroy(v), 0);
}

TEST(variant, function, ctor) {
	variant * args[1];
	args[0] = varfromfunc(&test_function);
	NOT_EQUAL(args[0], NULL);

	variant * v = varcreate(varfunctype(), 1, args);
	NOT_EQUAL(v, NULL);

	variant_function func1 = vartofunc(args[0]);
	variant_function func2 = vartofunc(v);
	EQUAL(func1, func2);

	EQUAL(vardestroy(args[0]), 0);
	EQUAL(vardestroy(v), 0);
}

TEST(variant, function, ctor_error) {
	variant * args[1];
	args[0] = varfromint(1);
	NOT_EQUAL(args[0], NULL);

	variant * v = varcreate(varfunctype(), 1, args);
	EQUAL(v, NULL);

	EQUAL(vardestroy(args[0]), 0);
}

TEST(variant, function, static) {
	struct function_variant v = varfromfunc_static(&test_function);

	variant_function func = vartofunc((variant *) &v);
	NOT_EQUAL(func, NULL);

	variant * result = func(NULL, 0, NULL);
	NOT_EQUAL(result, NULL);

	long long int int_result = vartoint(result);
	EQUAL(int_result, 1);

	EQUAL(vardestroy(result), 0);
}
