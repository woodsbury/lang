#include <common/hash.h>

extern inline uint64_t hash_fetch64(unsigned char const * src);
extern inline uint32_t hash_fetch32(unsigned char const * src);
extern inline uint16_t hash_fetch16(unsigned char const * src);

extern inline uint64_t hash64(unsigned char const * src,
                              size_t len);
