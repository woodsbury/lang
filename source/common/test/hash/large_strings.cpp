#include <cstring>

#include <test/benchmark.hpp>
#include <test/unit.hpp>

#include <common/hash.h>

TEST(common, hash, large_strings, diff_first_letter) {
	char const * str1 = "abcdefghijklmnopqrstuvwxyz";
	char const * str2 = "bbcdefghijklmnopqrstuvwxyz";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));

	NOT_EQUAL(hash1, hash2);
}

TEST(common, hash, large_strings, diff_last_letter) {
	char const * str1 = "abcdefghijklmnopqrstuvwxyz";
	char const * str2 = "abcdefghijklmnopqrstuvwxya";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));

	NOT_EQUAL(hash1, hash2);
}

TEST(common, hash, large_strings, diff_length) {
	char const * str1 = "abcdefghijklmnopqrstuvwxy";
	char const * str2 = "abcdefghijklmnopqrstuvwxyz";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));

	NOT_EQUAL(hash1, hash2);
}

TEST(common, hash, large_strings, check_alignment) {
	char const * str1 = "abcdefghijklmnopqrstuvwxyz";
	char const * str2 = "xabcdefghijklmnopqrstuvwxyzx";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));
	uint64_t hash3 = hash64((unsigned char const *) &str2[1],
	                        std::strlen(str1));

	NOT_EQUAL(hash1, hash2);
	EQUAL(hash1, hash3);
}


TEST(common, hash, large_strings, numbers) {
	char const * str1 = "1234567890";
	char const * str2 = "0987654321";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));

	NOT_EQUAL(hash1, hash2);
}


TEST(common, hash, large_strings, benchmark) {
	char const * str = "abcdefghijklmnopqrstuvwxyz1234567890";

	BENCHMARK {
		uint64_t result = hash64((unsigned char const *) str,
		                         std::strlen(str));
	}
}
