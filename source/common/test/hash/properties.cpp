#include <bitset>
#include <vector>

#include <test/unit.hpp>

#include <platform/compiler.h>

#include <common/hash.h>

TEST(common, hash, properties, check_avalanche) {
	unsigned int const in_size = 21;
	unsigned int const bit_count = in_size * 8;
	unsigned int const target_changes = bit_count / 3;

	unsigned char in[in_size];

	for (int i = 0; i < in_size; ++i) {
		in[i] = (i * 2) % 255;
	}

	uint64_t orighash = hash64(in, in_size);

	std::vector<unsigned int> changes(bit_count);

	for (int i = 0; i < in_size; ++i) {
		for (int j = 0; j < sizeof(uint64_t); ++j) {
			unsigned char bit = 1 << j;

			in[i] ^= bit;
			uint64_t newhash = hash64(in, in_size);
			in[i] ^= j;

			std::bitset<64> diff = orighash ^ newhash;

			GREATER_THAN(diff.count(), sizeof(uint64_t) * 8 / 4);

			for (int k = 0; k < bit_count; ++k) {
				if (diff[k]) {
					++(changes[k]);
				}
			}
		}
	}

	for (int i = 0; i < bit_count; ++i) {
		GREATER_THAN(changes[i], target_changes);
	}
}
