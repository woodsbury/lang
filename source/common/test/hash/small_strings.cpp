#include <cstring>

#include <test/benchmark.hpp>
#include <test/unit.hpp>

#include <common/hash.h>

TEST(common, hash, small_strings, diff_first_letter) {
	char const * str1 = "abcd";
	char const * str2 = "bbcd";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));

	NOT_EQUAL(hash1, hash2);
}

TEST(common, hash, small_strings, diff_last_letter) {
	char const * str1 = "abcd";
	char const * str2 = "abcc";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));

	NOT_EQUAL(hash1, hash2);
}

TEST(common, hash, small_strings, diff_length) {
	char const * str1 = "abcd";
	char const * str2 = "abcde";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));

	NOT_EQUAL(hash1, hash2);
}

TEST(common, hash, small_strings, check_alignment) {
	char const * str1 = "abcd";
	char const * str2 = "xabcdx";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));
	uint64_t hash3 = hash64((unsigned char const *) &str2[1],
	                        std::strlen(str1));

	NOT_EQUAL(hash1, hash2);
	EQUAL(hash1, hash3);
}

TEST(common, hash, small_strings, numbers) {
	char const * str1 = "1234";
	char const * str2 = "5678";

	uint64_t hash1 = hash64((unsigned char const *) str1,
	                        std::strlen(str1));
	uint64_t hash2 = hash64((unsigned char const *) str2,
	                        std::strlen(str2));

	NOT_EQUAL(hash1, hash2);
}

TEST(common, hash, small_strings, benchmark) {
	char const * str = "abcdef";

	BENCHMARK {
		uint64_t result = hash64((unsigned char const *) str,
		                         std::strlen(str));
	}
}
