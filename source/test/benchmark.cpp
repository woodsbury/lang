#include <sstream>
#include <sys/time.h>

#include <platform/compiler.h>

#include <test/benchmark.hpp>

#define MAX_ITERATIONS 2000000000

namespace test{
namespace {
inline unsigned long long current_time() {
	timeval time;
	gettimeofday(&time, 0);

	return time.tv_usec + time.tv_sec * 1000000ull;
}
}

benchmark_result::benchmark_result(unsigned int iterations,
                                   long double duration)
: iterations(iterations), duration(duration) {}

std::string benchmark_result::message() const {
	std::stringstream stream;

	stream << duration / iterations << " secs avg ("
	       << iterations << " iterations in "
	       << duration << " secs)";

	return stream.str();
}

benchmark_iterator::benchmark_iterator() : iterations(1),
                                           target_iterations(1024),
                                           start(current_time()) {}

bool benchmark_iterator::end(test * test_obj) {
	if (likely(iterations < target_iterations)) {
		return false;
	}

	unsigned long long stop = current_time();

	if (((stop - start) > 50000ull) ||
	    (iterations > MAX_ITERATIONS)) {
		long double duration = (stop - start) / 1000000.0;

		test_obj->set_result(new benchmark_result(iterations,
							  duration));

		return true;
	}

	target_iterations *= 2;
	return false;
}

void benchmark_iterator::operator++() {
	++iterations;
}
}

TEST(benchmark, empty) {
	BENCHMARK {
		;
	}
}
