#include <test/test.hpp>

#include <iostream>
#include <list>
#include <map>
#include <string>
#include <utility>

namespace test {
typedef std::map<std::string, test *> test_list;

namespace {
int run_each_test(test_list::iterator begin, test_list::iterator end) {
	unsigned int total = 0;
	unsigned int successful = 0;

	std::list<std::pair<std::string, result *> > results;

	test_list::iterator i;
	for (i = begin; i != end; ++i) {
		std::cout << "Running " << i->first << ": ";

		test * test_obj = i->second;
		(*test_obj)();

		result * res = test_obj->get_result();
		std::cout << res->message() << std::endl;

		++total;

		if (res->successful()) {
			++successful;
		}

		if (res->reportable()) {
			results.push_back(std::make_pair(test_obj->get_name(),
			                                 res));
		}
	}

	std::cout << std::endl << "Results: " << std::endl;

	std::list<std::pair<std::string, result *> >::iterator j;
	for (j = results.begin(); j != results.end(); ++j) {
		result * res = j->second;

		std::cout << std::endl << j->first << ": "
		          << (res->successful() ? "Succeeded" : "Failed")
		          << std::endl << res->message() << std::endl;
	}

	std::cout << std::endl << successful << "/" << total
	          << " passed" << std::endl;

	return total - successful;
}
}

test_list & all_tests() {
	static test_list list;
	return list;
}

test::test(std::string name, test_func func) : name(name),
                                               func(func),
                                               res(new success_result) {
	test_list & list = all_tests();
	list.insert(std::make_pair(name, this));
}

test::~test() {
	delete res;
}

std::string test::get_name() const {
	return name;
}

result * test::get_result() const {
	return res;
}

void test::set_result(result * res) {
	if (this->res->reportable()) {
		// Already have a reportable result
		delete res;
	} else {
		result * old_res = this->res;
		this->res = res;
		delete old_res;
	}
}

void test::operator()() {
	func(this);
}

int run_test(std::string name) {
	test_list & list = all_tests();

	test_list::iterator begin = list.lower_bound(name);

	test_list::iterator end;
	for (end = begin; end != list.end(); ++end) {
		if (end->first.find(name) != 0) {
			break;
		}
	}

	return run_each_test(begin, end);
}

int run_all_tests() {
	test_list & list = all_tests();

	return run_each_test(list.begin(), list.end());
}
}
