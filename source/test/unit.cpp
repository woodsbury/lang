#include <test/unit.hpp>

namespace test{
equal_result::equal_result(char const * x, char const * y)
	: x(x), y(y) {}

std::string equal_result::message() const {
	return std::string(x) + " not equal to " + std::string(y);
}

void assert_equal(test * test_obj, char const * x, char const * y,
                  int result) {
	if (!result) {
		test_obj->set_result(new equal_result(x, y));
	}
}

not_equal_result::not_equal_result(char const * x, char const * y)
	: x(x), y(y) {}

std::string not_equal_result::message() const {
	return std::string(x) + " equal to " + std::string(y);
}

void assert_not_equal(test * test_obj, char const * x, char const * y,
                      int result) {
	if (!result) {
		test_obj->set_result(new not_equal_result(x, y));
	}
}

greater_than_result::greater_than_result(char const * x, char const * y)
	: x(x), y(y) {}

std::string greater_than_result::message() const {
	return std::string(x) + " is less than " + std::string(y);
}

void assert_greater_than(test * test_obj, char const * x, char const * y,
                         int result) {
	if (!result) {
		test_obj->set_result(new greater_than_result(x, y));
	}
}

less_than_result::less_than_result(char const * x, char const * y)
	: x(x), y(y) {}

std::string less_than_result::message() const {
	return std::string(x) + " is greater than " + std::string(y);
}

void assert_less_than(test * test_obj, char const * x, char const * y,
                      int result) {
	if (!result) {
		test_obj->set_result(new less_than_result(x, y));
	}
}
}
