#include <platform/arch.h>

#if !defined(COMPILER_CLANG) && !defined(COMPILER_GCC)

extern inline unsigned int popcount(unsigned long long x);

#endif
