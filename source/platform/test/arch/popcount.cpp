#include <test/unit.hpp>

#include <platform/arch.h>

TEST(platform, compiler, popcount_8bit) {
	unsigned long long a = 0xFF;
	unsigned long long b = 0x0F;
	unsigned long long c = 0xF0;

	EQUAL(popcount(a), 8);
	EQUAL(popcount(b), 4);
	EQUAL(popcount(c), 4);
}

TEST(platform, compiler, popcount_16bit) {
	unsigned long long a = 0xFFFF;
	unsigned long long b = 0x00FF;
	unsigned long long c = 0xFF00;

	EQUAL(popcount(a), 16);
	EQUAL(popcount(b), 8);
	EQUAL(popcount(c), 8);
}

TEST(platform, compiler, popcount_32bit) {
	unsigned long long a = 0xFFFFFFFF;
	unsigned long long b = 0x0000FFFF;
	unsigned long long c = 0xFFFF0000;

	EQUAL(popcount(a), 32);
	EQUAL(popcount(b), 16);
	EQUAL(popcount(c), 16);
}

TEST(platform, compiler, popcount_64bit) {
	unsigned long long a = 0xFFFFFFFFFFFFFFFF;
	unsigned long long b = 0x00000000FFFFFFFF;
	unsigned long long c = 0xFFFFFFFF00000000;

	EQUAL(popcount(a), 64);
	EQUAL(popcount(b), 32);
	EQUAL(popcount(c), 32);
}
