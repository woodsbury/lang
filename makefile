### Options
DEBUG ?= 1
ifeq ($(DEBUG), 1)
	CFLAGS += -g3
else
	CFLAGS += -O3
	CXXFLAGS += -O3
	CPPFLAGS += -DNDEBUG
endif

### Common values
CPPFLAGS += -Iinclude -Ibuild/include -Itools
CFLAGS += -std=c99

# All normal source files
SOURCES := $(shell find source \( -name test -prune \) -o -type f -name \*.c -print)
SOURCES += $(shell find source \( -name test -prune \) -o -type f -name \*.cpp -print)
OBJECTS := $(patsubst %.c, build/%.o, $(SOURCES))
OBJECTS := $(patsubst %.cpp, build/%.o, $(OBJECTS))

# All normal source files, and test source files
SOURCES := $(shell find source -type f -name \*.c)
SOURCES += $(shell find source -type f -name \*.cpp)
TEST_OBJECTS := $(patsubst %.c, build/%.o, $(SOURCES))
TEST_OBJECTS := $(patsubst %.cpp, build/%.o, $(TEST_OBJECTS))

# All normal, test, and tool source files
SOURCES += $(shell find tools -type f -name \*.c)
SOURCES += $(shell find tools -type f -name \*.cpp)
ALL_OBJECTS := $(patsubst %.c, build/%.o, $(SOURCES))
ALL_OBJECTS := $(patsubst %.cpp, build/%.o, $(SOURCES))

DIRS := $(shell find source -type d)
DIRS += $(shell find tools -type d)
DIRS := $(patsubst %, build/%, $(DIRS))

### Phony all target
.PHONY : all
all : $(ALL_OBJECTS)

.PHONY : echo_objs
echo_objs :
	echo $(OBJECTS)
	echo $(TEST_OBJECTS)
	echo $(ALL_OBJECTS)

### Include other makefiles
MAKEFILES := $(shell find . -type f -name \*.mk)

include $(MAKEFILES)

.PHONY : mkfiles
mkfiles :
	@echo $(MAKEFILES)

### Include dependency files
DEPENDS := $(patsubst %.c, build/%.d, $(SOURCES))
DEPENDS := $(patsubst %.cpp, build/%.d, $(DEPENDS))

-include $(DEPENDS)

### Common rules

# Build directories
$(ALL_OBJECTS) : | $(DIRS)

$(DIRS) :
	mkdir -p $@

# build/include directory
build/include :
	mkdir -p build/include

# C source file
build/%.o : %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) $(DEBUG_FLAGS) $(LDFLAGS) $< -c -o $@ -MMD

# C++ source file
build/%.o : %.cpp
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(DEBUG_FLAGS) $(LDFLAGS) $< -c -o $@ -MMD

# Clean up the build directory
.PHONY : clean
clean :
	-rm -rf build
