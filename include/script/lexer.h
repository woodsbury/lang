#ifndef SCRIPT_LEXER_H
#define SCRIPT_LEXER_H

#ifdef __cplusplus
extern "C" {
#define restrict
#endif

enum token_type {
	T_ERROR_UNEXPECTED = -1,
	T_ERROR_UNTERM_COMMENT = -2,
	T_ERROR_UNTERM_REGEX = -3,
	T_ERROR_UNTERM_STRING = -4,
	T_ERROR_UNMATCH_CLOSE_PAREN = -5,
	T_ERROR_UNMATCH_OPEN_PAREN = -6,
	T_ERROR_PAREN_DEPTH = -7,

	T_END = 0,

	T_COMMENT,
	T_DECIMAL,
	T_IDENTIFIER,
	T_INTEGER,
	T_NUMBER,
	T_REGEX,
	T_STRING,

	T_BREAK,
	T_CASE,
	T_CATCH,
	T_CLASS,
	T_CONTINUE,
	T_DEFAULT,
	T_DO,
	T_ELSE,
	T_ENUM,
	T_FALSE,
	T_FINALLY,
	T_FOR,
	T_FUNCTION,
	T_IF,
	T_IMPORT,
	T_NAMESPACE,
	T_NEW,
	T_NULL,
	T_RETURN,
	T_SWITCH,
	T_THIS,
	T_THROW,
	T_TRUE,
	T_TRY,
	T_TYPEOF,
	T_VAR,
	T_WHILE,

	T_ADD,
	T_ADD_ASSIGN,
	T_AND,
	T_ASSIGN,
	T_BIT_AND,
	T_BIT_AND_ASSIGN,
	T_BIT_NOT,
	T_BIT_OR,
	T_BIT_OR_ASSIGN,
	T_BIT_XOR,
	T_BIT_XOR_ASSIGN,
	T_CLOSE_BRACE,
	T_CLOSE_PAREN,
	T_CLOSE_SQUARE_BRACE,
	T_COLON,
	T_COMMA,
	T_DIVIDE,
	T_DIVIDE_ASSIGN,
	T_EQUAL,
	T_GREATER,
	T_GREATER_OR_EQUAL,
	T_IDENTICAL,
	T_LEFT_SHIFT,
	T_LEFT_SHIFT_ASSIGN,
	T_LESS,
	T_LESS_OR_EQUAL,
	T_MODULUS,
	T_MODULUS_ASSIGN,
	T_MULTIPLY,
	T_MULTIPLY_ASSIGN,
	T_NEGATIVE,
	T_NOT,
	T_NOT_EQUAL,
	T_NOT_IDENTICAL,
	T_OPEN_BRACE,
	T_OPEN_PAREN,
	T_OPEN_SQUARE_BRACE,
	T_OR,
	T_PERIOD,
	T_POST_DECREMENT,
	T_POST_INCREMENT,
	T_PRE_DECREMENT,
	T_PRE_INCREMENT,
	T_RIGHT_SHIFT,
	T_RIGHT_SHIFT_ASSIGN,
	T_RIGHT_SHIFT_ZERO,
	T_RIGHT_SHIFT_ZERO_ASSIGN,
	T_SEMICOLON,
	T_SHORT_IF,
	T_SUBTRACT,
	T_SUBTRACT_ASSIGN
};

struct lexer {
	char const * start;
	char const * position;

	unsigned int line;

	unsigned int paren_depth : 30;

	unsigned int expect_block : 1;
	unsigned int expect_value : 1;
};

struct token {
	enum token_type type;

	char const * start;
	char const * end;
	unsigned int line;
};

struct lexer lexstr(char const * str);
struct token lex(struct lexer * l);

int lexerror(struct lexer const * restrict l, struct token const * restrict t,
             char const * restrict filename);
void lexmsg(struct lexer const * restrict l, struct token const * restrict t,
            char const * restrict msg, char const * restrict filename);

#ifdef __cplusplus
#undef restrict
}
#endif

#endif
