#ifndef VARIANT_VALUE_H
#define VARIANT_VALUE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct variant_base variant;
struct variant_type;

typedef variant * (* variant_function)(variant * self, size_t nargs,
                                       variant * args[]);

variant * varcreate(struct variant_type * type, size_t nargs, variant * args[]);
int vardestroy(variant * v);

struct variant_type * vartype(variant const * v);
char const * vartypename(struct variant_type * type);

variant * varfromfloat(double val);
double vartofloat(variant const * v);

variant * varfromfunc(variant_function func);
variant_function vartofunc(variant const * v);

variant * varfromint(int64_t val);
int64_t vartoint(variant const * v);

#ifdef __cplusplus
}
#endif

#endif
