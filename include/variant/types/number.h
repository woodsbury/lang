#ifndef VARIANT_TYPES_NUMBER_H
#define VARIANT_TYPES_NUMBER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <variant/types/base.h>

extern struct variant_type number_variant_type;

struct number_variant {
	struct variant_base base;

	double value;
};

#define varfromfloat_static(val) (struct number_variant) \
                                 { { &number_variant_type }, (val) }

inline struct variant_type * varnumtype(void) {
	return &number_variant_type;
}

#ifdef __cplusplus
}
#endif

#endif
