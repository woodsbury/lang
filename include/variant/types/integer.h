#ifndef VARIANT_TYPES_INTEGER_H
#define VARIANT_TYPES_INTEGER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#include <variant/types/base.h>

extern struct variant_type integer_variant_type;

struct integer_variant {
	struct variant_base base;

	int64_t value;
};

#define varfromint_static(val) (struct integer_variant) \
                               { { &integer_variant_type }, (val) }

inline struct variant_type * varinttype(void) {
	return &integer_variant_type;
}

#ifdef __cplusplus
}
#endif

#endif
