#ifndef VARIANT_TYPES_FUNCTION_H
#define VARIANT_TYPES_FUNCTION_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

#include <variant/value.h>
#include <variant/types/base.h>

extern struct variant_type function_variant_type;

struct function_variant {
	struct variant_base base;

	variant_function function;
};

#define varfromfunc_static(func) (struct function_variant) \
                                 { { &function_variant_type }, (func) }

inline struct variant_type * varfunctype(void) {
	return &function_variant_type;
}

#ifdef __cplusplus
}
#endif

#endif
