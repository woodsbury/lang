#ifndef VARIANT_TYPES_BASE_H
#define VARIANT_TYPES_BASE_H

#ifdef __cplusplus
extern "C" {
#endif

struct variant_base {
	struct variant_type * type;
};

#ifdef __cplusplus
}
#endif

#endif
