#ifndef VARIANT_TYPE_H
#define VARIANT_TYPE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

#include <variant/value.h>

typedef int (* type_init)(struct variant_type * type);
typedef int (* type_deinit)(struct variant_type * type);

typedef int (* variant_ctor)(variant * self, size_t nargs, variant * args[]);
typedef int (* variant_dtor)(variant * self);

struct variant_type {
	char const * name;

	size_t size;

	type_init init;
	type_deinit deinit;

	variant_ctor ctor;
	variant_dtor dtor;
};

#ifdef __cplusplus
}
#endif

#endif
