#ifndef PLATFORM_ARCH_H
#define PLATFORM_ARCH_H

#ifdef __cplusplus
extern "C" {
#endif

#include <platform/compiler.h>

#if defined(COMPILER_CLANG) || defined(COMPILER_GCC)

#define popcount(x) __builtin_popcountll(x)

#else

inline unsigned int popcount(unsigned long long x) {
	unsigned long long const b1 = 0x5555555555555555ull;
	unsigned long long const b2 = 0x3333333333333333ull;
	unsigned long long const b3 = 0x0f0f0f0f0f0f0f0full;
	unsigned long long const b4 = 0x0101010101010101ull;

	x -= (x >> 1) & b1;
	x = (x & b2) + ((x >> 2) & b2);
	x = (x + (x >> 4)) & b3;
	return (x * b4) >> 56;
}

#endif

#ifdef __cplusplus
}
#endif

#endif
