#ifndef PLATFORM_COMPILER_H
#define PLATFORM_COMPILER_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__clang__)
#define COMPILER_CLANG
#elif defined(__GNUC__)
#define COMPILER_GCC
#endif

#if defined(COMPILER_CLANG) || defined(COMPILER_GCC)

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

#define prefetch(x) __builtin_prefetch(x)

#else

#define likely(x) (x)
#define unlikely(x) (x)

#define prefetch(x)

#endif

#ifdef __cplusplus
}
#endif

#endif
