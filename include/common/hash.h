#ifndef COMMON_HASH_H
#define COMMON_HASH_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <string.h>

inline uint64_t hash_fetch64(unsigned char const * src) {
	uint64_t result;
	memcpy(&result, src, sizeof(result));
	return result;
}

inline uint32_t hash_fetch32(unsigned char const * src) {
	uint32_t result;
	memcpy(&result, src, sizeof(result));
	return result;
}

inline uint16_t hash_fetch16(unsigned char const * src) {
	uint16_t result;
	memcpy(&result, src, sizeof(result));
	return result;
}

inline uint64_t hash64(unsigned char const * src,
                       size_t len) {
	uint64_t hash = 0xc3a5c85c97cb3127ull;

	while (len >= sizeof(uint64_t)) {
		hash ^= hash_fetch64(src + len - sizeof(uint64_t));
		len -= sizeof(uint64_t);
	}

	if (len >= sizeof(uint32_t)) {
		hash ^= hash_fetch32(src + len - sizeof(uint32_t));
		hash ^= hash << 32;
		len -= sizeof(uint32_t);
	}

	if (len >= sizeof(uint16_t)) {
		hash ^= hash_fetch16(src + len - sizeof(uint16_t));
		hash ^= (hash << 24) ^ (hash << 56);
		len -= sizeof(uint16_t);
	}

	if (len > 0) {
		hash ^= src[0];
	}

	hash += (hash >> 33) * hash;
	hash ^= hash >> 47;

	return hash;
}

#ifdef __cplusplus
}
#endif

#endif
