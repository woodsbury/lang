#ifndef RESULT_HPP
#define RESULT_HPP

#include <string>

namespace test {

class result {
public:
	virtual ~result() {}

	virtual std::string message() const = 0;

	virtual bool reportable() const {
		return true;
	}

	virtual bool successful() const {
		return false;
	}
};

class success_result : public result {
public:
	virtual std::string message() const {
		return "Success";
	}

	virtual bool reportable() const {
		return false;
	}

	virtual bool successful() const {
		return true;
	}
};

class failure_result : public result {
public:
	virtual std::string message() const {
		return "Failure";
	}
};

}

#endif
