#ifndef BENCHMARK_HPP
#define BENCHMARK_HPP

#include <test/test.hpp>

#define BENCHMARK for (test::benchmark_iterator i; !i.end(test_obj); ++i)

namespace test {
class benchmark_result : public result {
	unsigned int iterations;
	long double duration;

public:
	benchmark_result(unsigned int iterations, long double duration);

	virtual std::string message() const;

	virtual bool successful() const {
		return true;
	}
};

class benchmark_iterator {
	unsigned long long iterations;
	unsigned long long target_iterations;

	unsigned long long start;

public:
	benchmark_iterator();

	bool end(test * test_obj);

	void operator++();
};
}

#endif
