#ifndef UNIT_HPP
#define UNIT_HPP

#include <cfloat>
#include <cmath>
#include <cstring>

#include <test/test.hpp>

namespace test {
class equal_result : public result {
	char const * x;
	char const * y;

public:
	equal_result(char const * x, char const * y);

	virtual std::string message() const;
};

void assert_equal(test * test_obj, char const * x, char const * y,
                  int result);

class not_equal_result : public result {
	char const * x;
	char const * y;

public:
	not_equal_result(char const * x, char const * y);

	virtual std::string message() const;
};

void assert_not_equal(test * test_obj, char const * x, char const * y,
                      int result);

class greater_than_result : public result {
	char const * x;
	char const * y;

public:
	greater_than_result(char const * x, char const * y);

	virtual std::string message() const;
};

void assert_greater_than(test * test_obj, char const * x, char const * y,
                         int result);

class less_than_result : public result {
	char const * x;
	char const * y;

public:
	less_than_result(char const * x, char const * y);

	virtual std::string message() const;
};

void assert_less_than(test * test_obj, char const * x, char const * y,
                      int result);
}

#define EQUAL(x, y) test::assert_equal(test_obj, #x, #y, \
                                       ((x) == (y)))
#define NOT_EQUAL(x, y) test::assert_not_equal(test_obj, #x, #y, \
                                               ((x) != (y)))

#define STR_EQUAL(x, y) test::assert_equal(test_obj, #x, #y, \
                                           std::strcmp(x, y) == 0)
#define STR_NOT_EQUAL(x, y) test::assert_not_equal(test_obj, #x, #y, \
                                                   std::strcmp(x, y) != 0)

#define FLT_EQUAL(x, y) test::assert_equal(test_obj, #x, #y, \
                                           fabs(x - y) < FLT_EPSILON)
#define FLT_NOT_EQUAL(x, y) test::assert_not_equal(test_obj, #x, #y, \
                                                   fabs(x - y) > FLT_EPSILON)

#define GREATER_THAN(x, y) test::assert_greater_than(test_obj, #x, #y, \
                                                     ((x) > (y)))
#define LESS_THAN(x, y) test::assert_less_than(test_obj, #x, #y, \
                                               ((x) < (y)))

#endif
