#ifndef TEST_HPP
#define TEST_HPP

#include <string>

#include <test/result.hpp>

#define TEST_CAT_F1(a) a
#define TEST_CAT_F2(a, b) a##__##b
#define TEST_CAT_F3(a, b, c) \
	a##__##b##__##c
#define TEST_CAT_F4(a, b, c, d) \
	a##__##b##__##c##__##d
#define TEST_CAT_F5(a, b, c, d, e) \
	a##__##b##__##c##__##d##__##e
#define TEST_CAT_F6(a, b, c, d, e, f) \
	a##__##b##__##c##__##d##__##e##__##f
#define TEST_CAT_F7(a, b, c, d, e, f, g) \
	a##__##b##__##c##__##d##__##e##__##f##__##g

#define TEST_CAT_S1(a) #a
#define TEST_CAT_S2(a, b) #a "::" #b
#define TEST_CAT_S3(a, b, c) \
	#a "::" #b "::" #c
#define TEST_CAT_S4(a, b, c, d) \
	#a "::" #b "::" #c "::" #d
#define TEST_CAT_S5(a, b, c, d, e) \
#a "::" #b "::" #c "::" #d "::" #e
#define TEST_CAT_S6(a, b, c, d, e, f) \
	#a "::" #b "::" #c "::" #d "::" #e "::" #f
#define TEST_CAT_S7(a, b, c, d, e, f, g) \
	#a "::" #b "::" #c "::" #d "::" #e "::" #f "::" #g

#define TEST_FUNC(...) TEST_CAT_F(__VA_ARGS__)
#define TEST_CAT_F(...) \
	TEST_CALL(__VA_ARGS__, TEST_CAT_F7, \
	          TEST_CAT_F6, TEST_CAT_F5, \
	          TEST_CAT_F4, TEST_CAT_F3, \
	          TEST_CAT_F2, TEST_CAT_F1) (__VA_ARGS__)

#define TEST_STR(...) TEST_CAT_S(__VA_ARGS__)
#define TEST_CAT_S(...) \
	TEST_CALL(__VA_ARGS__, TEST_CAT_S7, \
	          TEST_CAT_S6, TEST_CAT_S5, \
	          TEST_CAT_S4, TEST_CAT_S3, \
	          TEST_CAT_S2, TEST_CAT_S1)(__VA_ARGS__)

#define TEST_CALL(_1, _2, _3, _4, _5, _6, _7, N, ...) N

#define TEST(...) \
	void TEST_FUNC(__VA_ARGS__)(test::test * test_obj); \
	static test::test TEST_FUNC(__VA_ARGS__, obj)(TEST_STR(__VA_ARGS__), \
	                                             TEST_FUNC(__VA_ARGS__)); \
	void TEST_FUNC(__VA_ARGS__)(test::test * test_obj)

namespace test {
class test {
	typedef void (*test_func)(test *);

	std::string name;
	test_func func;

	result * res;

public:
	test(std::string name, test_func func);
	~test();

	std::string get_name() const;

	void set_result(result * res);
	result * get_result() const;

	void operator()();
};

int run_test(std::string name);
int run_all_tests();
}

#endif
