all : hashprint

hashprint : build/tools/hashprint/hashprint.o build/source/common/hash.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LOADLIBES) -o $@
