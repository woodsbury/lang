#include <inttypes.h>
#include <stdio.h>
#include <string.h>

#include <common/hash.h>

int main(int argc, char * argv[]) {
	if (argc < 2) {
		return 1;
	}

	for (int i = 1; i < argc; ++i) {
		uint64_t result = hash64((unsigned char const *) argv[i],
		                         strlen(argv[i]));

		printf("%s : 0x%" PRIx64 " (%" PRIu64 ")\n",
		       argv[i], result, result);
	}

	return 0;
}
