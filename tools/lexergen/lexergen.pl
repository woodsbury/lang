use Data::Dumper;

my $FIRST_CHAR = ord('!');
my $LAST_CHAR = ord('~');
my $ROW_SIZE = $LAST_CHAR - $FIRST_CHAR + 1;

my $END_STATE = -1;

sub idx {
	my $character = shift || $_;

	if (ord($character) < $FIRST_CHAR) {
		print STDERR "'$character' (", ord($character),
		             ") < offset (", $FIRST_CHAR, ")\n";
	}

	return ord($character) - $FIRST_CHAR;
}

sub add_to_table {
	my $table = shift;
	my $index = shift;
	my $character = shift || $_;

	if (!defined($table->[$index][idx($character)])) {
		push @$table, [];
		$table->[$index][idx($character)] = $#$table;
	}

	return $table->[$index][idx($character)];
}

my @lextable;
my @lexmap;

$lexmap[0] = 'T_ERROR_UNEXPECTED';

my @input = <DATA>;

# Identifiers and keywords
push @lextable, [];

while ($#input >= 0) {
	my $line = shift @input;
	chomp $line;

	if ($line eq '') {
		last;
	}

	my @keyword = split ' ', $line, 2;
	my @letters = split '', $keyword[1];
	my $index = 0;

	for (@letters) {
		$index = add_to_table(\@lextable, $index);
	}

	$lexmap[$index] = $keyword[0];
}

push @lextable, [];

for ('a'..'z', 'A'..'Z', '0'..'9', '_') {
	for my $row (@lextable) {
		if ($row->[idx()] == undef) {
			$row->[idx()] = $#lextable;
		}
	}
}

for my $tok (@lexmap) {
	if ($tok eq undef) {
		$tok = 'T_IDENTIFIER';
	}
}

$lexmap[$#lextable] = 'T_IDENTIFIER';

# Operators
for (@input) {
	chomp;
	my @operator = split ' ', $_, 2;
	my @characters = split '', $operator[1];
	my $index = 0;

	for (@characters) {
		$index = add_to_table(\@lextable, $index);
	}

	$lexmap[$index] = $operator[0];
}

# Numbers
push @lextable, [];
push @lextable, [];
push @lextable, [];

for ('0'..'9') {
	$lextable[0][idx()] = $#lextable - 2;
	$lextable[$#lextable - 2][idx()] = $#lextable - 2;
	$lextable[$#lextable - 1][idx()] = $#lextable - 1;
	$lextable[$#lextable][idx()] = $#lextable;
}

$lextable[$#lextable - 2][idx('.')] = $#lextable - 1;
$lextable[$#lextable - 2][idx('e')] = $#lextable;
$lextable[$#lextable - 1][idx('e')] = $#lextable;

$lexmap[$#lextable - 2] = 'T_INTEGER';
$lexmap[$#lextable - 1] = 'T_DECIMAL';
$lexmap[$#lextable] = 'T_DECIMAL';

push @lextable, [];

$lextable[$#lextable - 2][idx('f')] = $#lextable;
$lextable[$#lextable - 1][idx('f')] = $#lextable;

$lexmap[$#lextable] = 'T_NUMBER';

if ($#ARGV >= 0) {
	# Try to classify each argument
	for (@ARGV) {
		my $curr = 0;

		print "$_ : ";

		for (split '') {
			my $next = $lextable[$curr][idx()];

			if (defined($next)) {
				$curr = $next;
			} else {
				print "extra characters from '$_' + ";
				last;
			}
		}

		print "$lexmap[$curr]\n";
	}
} else {
	# Output the table
	my $num_rows = $#lextable + 1;

	print <<"END";
#ifndef LEXTABLE_H
#define LEXTABLE_H

#include <stdint.h>

#define idx(c) (c - $FIRST_CHAR)
#define out_of_range(c) (c < $FIRST_CHAR || c > $LAST_CHAR)

int16_t lextable[$num_rows][$ROW_SIZE] = {
END

	for my $row (0..$#lextable) {
		print "{";

		for (0..($ROW_SIZE - 1)) {
			if (defined($lextable[$row][$_])) {
				print $lextable[$row][$_];
			} else {
				print "-1";
			}

			print ", " unless $_ == ($ROW_SIZE - 1);
		}

		print "}";
		print ",\n" unless $row == $#lextable;
	}

	print <<"END";
};

enum token_type lexmap[] = {
END

	for my $row (0..$#lexmap) {
		print $lexmap[$row];
		print ",\n" unless $row == $#lexmap;
	}

	print <<"END";
};

#endif
END
}

__DATA__
T_BREAK break
T_CASE case
T_CATCH catch
T_CLASS class
T_CONTINUE continue
T_DEFAULT default
T_DO do
T_ELSE else
T_ENUM enum
T_FALSE false
T_FINALLY finally
T_FOR for
T_FUNCTION function
T_IF if
T_IMPORT import
T_NAMESPACE namespace
T_NEW new
T_NULL null
T_RETURN return
T_SWITCH switch
T_THIS this
T_THROW throw
T_TRUE true
T_TRY try
T_TYPEOF typeof
T_VAR var
T_WHILE while

T_ADD +
T_ADD_ASSIGN +=
T_AND &&
T_ASSIGN =
T_BIT_AND &
T_BIT_AND_ASSIGN &=
T_BIT_NOT ~
T_BIT_OR |
T_BIT_OR_ASSIGN |=
T_BIT_XOR ^
T_BIT_XOR_ASSIGN ^=
T_CLOSE_BRACE }
T_CLOSE_PAREN )
T_CLOSE_SQUARE_BRACE ]
T_COLON :
T_COMMA ,
T_DIVIDE /
T_DIVIDE_ASSIGN /=
T_EQUAL ==
T_GREATER >
T_GREATER_OR_EQUAL >=
T_IDENTICAL ===
T_LEFT_SHIFT <<
T_LEFT_SHIFT_ASSIGN <<=
T_LESS <
T_LESS_OR_EQUAL <=
T_MODULUS %
T_MODULUS_ASSIGN %=
T_MULTIPLY *
T_MULTIPLY_ASSIGN *=
T_NOT !
T_NOT_EQUAL !=
T_NOT_IDENTICAL !==
T_OPEN_BRACE {
T_OPEN_PAREN (
T_OPEN_SQUARE_BRACE [
T_OR ||
T_PERIOD .
T_POST_DECREMENT --
T_POST_INCREMENT ++
T_RIGHT_SHIFT >>
T_RIGHT_SHIFT_ASSIGN >>=
T_RIGHT_SHIFT_ZERO >>>
T_RIGHT_SHIFT_ZERO_ASSIGN >>>=
T_SEMICOLON ;
T_SHORT_IF ?
T_SUBTRACT -
T_SUBTRACT_ASSIGN -=
