all : tokenise

tokenise : build/tools/tokenise/tokenise.o build/source/script/lexer.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LOADLIBES) -o $@
