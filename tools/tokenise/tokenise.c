#include <stdio.h>

#include <script/lexer.h>

void print_token(struct token t);

int main(int argc, char * argv[]) {
	if (argc < 2) {
		return 1;
	}

	struct lexer l = lexstr(argv[1]);

	struct token t;
	do {
		t  = lex(&l);
		print_token(t);

		switch (t.type) {
		case T_COMMENT:
			printf(" : T_COMMENT\n");
			break;

		case T_DECIMAL:
			printf(" : T_DECIMAL\n");
			break;

		case T_INTEGER:
			printf(" : T_INTEGER\n");
			break;

		case T_NUMBER:
			printf(" : T_NUMBER\n");
			break;

		case T_REGEX:
			printf(" : T_REGEX\n");
			break;

		case T_STRING:
			printf(" : T_STRING\n");
			break;

		case T_IDENTIFIER:
			printf(" : T_IDENTIFIER\n");
			break;

		case T_BREAK:
			printf(" : T_BREAK\n");
			break;

		case T_CASE:
			printf(" : T_CASE\n");
			break;

		case T_CATCH:
			printf(" : T_CATCH\n");
			break;

		case T_CLASS:
			printf(" : T_CLASS\n");
			break;

		case T_CONTINUE:
			printf(" : T_CONTINUE\n");
			break;

		case T_DEFAULT:
			printf(" : T_DEFAULT\n");
			break;

		case T_DO:
			printf(" : T_DO\n");
			break;

		case T_ELSE:
			printf(" : T_ELSE\n");
			break;

		case T_ENUM:
			printf(" : T_ENUM\n");
			break;

		case T_FALSE:
			printf(" : T_FALSE\n");
			break;

		case T_FINALLY:
			printf(" : T_FINALLY\n");
			break;

		case T_FOR:
			printf(" : T_FOR\n");
			break;

		case T_FUNCTION:
			printf(" : T_FUNCTION\n");
			break;

		case T_IF:
			printf(" : T_IF\n");
			break;

		case T_IMPORT:
			printf(" : T_IMPORT\n");
			break;

		case T_NAMESPACE:
			printf(" : T_NAMESPACE\n");
			break;

		case T_NEW:
			printf(" : T_NEW\n");
			break;

		case T_NULL:
			printf(" : T_NULL\n");
			break;

		case T_RETURN:
			printf(" : T_RETURN\n");
			break;

		case T_SWITCH:
			printf(" : T_SWITCH\n");
			break;

		case T_THIS:
			printf(" : T_THIS\n");
			break;

		case T_THROW:
			printf(" : T_THROW\n");
			break;

		case T_TRUE:
			printf(" : T_TRUE\n");
			break;

		case T_TRY:
			printf(" : T_TRY\n");
			break;

		case T_TYPEOF:
			printf(" : T_TYPEOF\n");
			break;

		case T_VAR:
			printf(" : T_VAR\n");
			break;

		case T_WHILE:
			printf(" : T_WHILE\n");
			break;

		case T_ADD:
			printf(" : T_ADD\n");
			break;

		case T_ADD_ASSIGN:
			printf(" : T_ADD_ASSIGN\n");
			break;

		case T_AND:
			printf(" : T_AND\n");
			break;

		case T_ASSIGN:
			printf(" : T_ASSIGN\n");
			break;

		case T_BIT_AND:
			printf(" : T_BIT_AND\n");
			break;

		case T_BIT_AND_ASSIGN:
			printf(" : T_BIT_AND_ASSIGN\n");
			break;

		case T_BIT_NOT:
			printf(" : T_BIT_NOT\n");
			break;

		case T_BIT_OR:
			printf(" : T_BIT_OR\n");
			break;

		case T_BIT_OR_ASSIGN:
			printf(" : T_BIT_OR_ASSIGN\n");
			break;

		case T_BIT_XOR:
			printf(" : T_BIT_XOR\n");
			break;

		case T_BIT_XOR_ASSIGN:
			printf(" : T_BIT_XOR_ASSIGN\n");
			break;

		case T_CLOSE_BRACE:
			printf(" : T_CLOSE_BRACE\n");
			break;

		case T_CLOSE_PAREN:
			printf(" : T_CLOSE_PAREN, depth %u\n",
			       l.paren_depth);

			break;

		case T_CLOSE_SQUARE_BRACE:
			printf(" : T_CLOSE_SQUARE_BRACE\n");
			break;

		case T_COLON:
			printf(" : T_COLON\n");
			break;

		case T_COMMA:
			printf(" : T_COMMA\n");
			break;

		case T_DIVIDE:
			printf(" : T_DIVIDE\n");
			break;

		case T_DIVIDE_ASSIGN:
			printf(" : T_DIVIDE_ASSIGN\n");
			break;

		case T_EQUAL:
			printf(" : T_EQUAL\n");
			break;

		case T_GREATER:
			printf(" : T_GREATER\n");
			break;

		case T_GREATER_OR_EQUAL:
			printf(" : T_GREATER_OR_EQUAL\n");
			break;

		case T_IDENTICAL:
			printf(" : T_IDENTICAL\n");
			break;

		case T_LEFT_SHIFT:
			printf(" : T_LEFT_SHIFT\n");
			break;

		case T_LEFT_SHIFT_ASSIGN:
			printf(" : T_LEFT_SHIFT_ASSIGN\n");
			break;

		case T_LESS:
			printf(" : T_LESS\n");
			break;

		case T_LESS_OR_EQUAL:
			printf(" : T_LESS_OR_EQUAL");
			break;

		case T_MODULUS:
			printf(" : T_MODULUS\n");
			break;

		case T_MODULUS_ASSIGN:
			printf(" : T_MODULUS_ASSIGN\n");
			break;

		case T_MULTIPLY:
			printf(" : T_MULTIPLY\n");
			break;

		case T_MULTIPLY_ASSIGN:
			printf(" : T_MULTIPLY_ASSIGN\n");
			break;

		case T_NOT:
			printf(" : T_NOT\n");
			break;

		case T_NOT_EQUAL:
			printf(" : T_NOT_EQUAL\n");
			break;

		case T_NOT_IDENTICAL:
			printf(" : T_NOT_IDENTICAL\n");
			break;

		case T_OPEN_BRACE:
			printf(" : T_OPEN_BRACE\n");
			break;

		case T_OPEN_PAREN:
			printf(" : T_OPEN_PAREN - ");

			if (!l.expect_block) {
				printf("not ");
			}
\
			printf("expecting a block after, depth %u\n",
			       l.paren_depth);

			break;

		case T_OPEN_SQUARE_BRACE:
			printf(" : T_OPEN_SQUARE_BRACE\n");
			break;

		case T_OR:
			printf(" : T_OR\n");
			break;

		case T_PERIOD:
			printf(" : T_PERIOD\n");
			break;

		case T_POST_DECREMENT:
			printf(" : T_POST_DECREMENT\n");
			break;

		case T_POST_INCREMENT:
			printf(" : T_POST_INCREMENT\n");
			break;

		case T_PRE_DECREMENT:
			printf(" : T_PRE_DECREMENT\n");
			break;

		case T_PRE_INCREMENT:
			printf(" : T_PRE_INCREMENT\n");
			break;

		case T_RIGHT_SHIFT:
			printf(" : T_RIGHT_SHIFT\n");
			break;

		case T_RIGHT_SHIFT_ASSIGN:
			printf(" : T_RIGHT_SHIFT_ASSIGN\n");
			break;

		case T_RIGHT_SHIFT_ZERO:
			printf(" : T_RIGHT_SHIFT_ZERO\n");
			break;

		case T_RIGHT_SHIFT_ZERO_ASSIGN:
			printf(" : T_RIGHT_SHIFT_ZERO_ASSIGN\n");
			break;

		case T_SEMICOLON:
			printf(" : T_SEMICOLON\n");
			break;

		case T_SHORT_IF:
			printf(" : T_SHORT_IF\n");
			break;

		case T_SUBTRACT:
			printf(" : T_SUBTRACT\n");
			break;

		case T_SUBTRACT_ASSIGN:
			printf(" : T_SUBTRACT_ASSIGN\n");
			break;

		case T_END:
			break;

		default:
			if (t.type < 0) {
				lexerror(&l, &t, NULL);
			} else {
				printf(" : Unknown token type (%d)\n", t.type);
			}

			break;
		}
	} while (t.type != T_END);

	return 0;
}

void print_token(struct token t) {
	char const * position = t.start;

	while (position < t.end) {
		printf("%c", *position);
		++position;
	}
}
