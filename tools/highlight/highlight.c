#include <stdio.h>

#include <script/lexer.h>

int main(int argc, char * argv[]) {
	if (argc < 2) {
		return 1;
	}

	struct lexer l = lexstr(argv[1]);
	char const * curr = argv[1];

	struct token t;
	do {
		t  = lex(&l);

		while (curr < t.start) {
			printf("%c", *curr);
			++curr;
		}

		switch (t.type) {
		case T_COMMENT:
			printf("\033[38;5;22m");
			break;

		case T_DECIMAL:
		case T_INTEGER:
		case T_NUMBER:
			printf("\033[38;5;20m");
			break;

		case T_STRING:
			printf("\033[38;5;28m\033[1m");
			break;

		case T_IDENTIFIER:
			printf("\033[1m");
			break;

		case T_BREAK:
		case T_CASE:
		case T_CLASS:
		case T_CONTINUE:
		case T_DEFAULT:
		case T_DO:
		case T_ELSE:
		case T_ENUM:
		case T_FALSE:
		case T_FOR:
		case T_FUNCTION:
		case T_IF:
		case T_IMPORT:
		case T_NAMESPACE:
		case T_NEW:
		case T_NULL:
		case T_RETURN:
		case T_SWITCH:
		case T_THIS:
		case T_TRUE:
		case T_TYPEOF:
		case T_VAR:
		case T_WHILE:
			printf("\033[38;5;52m\033[1m");
			break;

		case T_ADD:
		case T_ADD_ASSIGN:
		case T_AND:
		case T_ASSIGN:
		case T_BIT_AND:
		case T_BIT_AND_ASSIGN:
		case T_BIT_NOT:
		case T_BIT_OR:
		case T_BIT_OR_ASSIGN:
		case T_BIT_XOR:
		case T_BIT_XOR_ASSIGN:
		case T_CLOSE_BRACE:
		case T_CLOSE_PAREN:
		case T_CLOSE_SQUARE_BRACE:
		case T_COLON:
		case T_COMMA:
		case T_DIVIDE:
		case T_DIVIDE_ASSIGN:
		case T_EQUAL:
		case T_GREATER:
		case T_GREATER_OR_EQUAL:
		case T_IDENTICAL:
		case T_LEFT_SHIFT:
		case T_LEFT_SHIFT_ASSIGN:
		case T_LESS:
		case T_LESS_OR_EQUAL:
		case T_MODULUS:
		case T_MODULUS_ASSIGN:
		case T_MULTIPLY:
		case T_MULTIPLY_ASSIGN:
		case T_NOT:
		case T_NOT_EQUAL:
		case T_NOT_IDENTICAL:
		case T_OPEN_BRACE:
		case T_OPEN_PAREN:
		case T_OPEN_SQUARE_BRACE:
		case T_OR:
		case T_PERIOD:
		case T_POST_DECREMENT:
		case T_POST_INCREMENT:
		case T_PRE_DECREMENT:
		case T_PRE_INCREMENT:
		case T_RIGHT_SHIFT:
		case T_RIGHT_SHIFT_ASSIGN:
		case T_RIGHT_SHIFT_ZERO:
		case T_RIGHT_SHIFT_ZERO_ASSIGN:
		case T_SEMICOLON:
		case T_SHORT_IF:
		case T_SUBTRACT:
		case T_SUBTRACT_ASSIGN:
			printf("\033[38;5;30m\033[1m");
			break;

		default:
			if (t.type < 0) {
				lexerror(&l, &t, NULL);
			}

			break;
		}

		while (curr < t.end) {
			printf("%c", *curr);
			++curr;
		}

		printf("\033[0m");
	} while (t.type != T_END);

	printf("\n");

	return 0;
}
