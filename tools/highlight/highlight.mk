all : highlight

highlight : build/tools/highlight/highlight.o build/source/script/lexer.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LOADLIBES) -o $@
