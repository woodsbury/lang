all : testrunner

testrunner : build/tools/testrunner/testrunner.o $(TEST_OBJECTS)
	$(CXX) $(CFLAGS) $(LDFLAGS) $^ $(LOADLIBES) -o $@
