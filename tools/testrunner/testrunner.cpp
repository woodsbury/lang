#include <test/test.hpp>

int main(int argc, char * argv[]) {
	int result = 0;

	if (argc > 1) {
		for (unsigned int i = 1; i < argc; ++i) {
			result += test::run_test(argv[i]);
		}
	} else {
		result = test::run_all_tests();
	}

	return result;
}
