#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <common/hash.h>
#include <platform/arch.h>

int main(int argc, char * argv[]) {
	if (argc < 3) {
		return 1;
	}

	uint64_t * results = malloc(sizeof(uint64_t) * (argc - 1));

	if (results == NULL) {
		perror(NULL);
		return 1;
	}

	unsigned int const num_mods = 30;

	unsigned int mods[num_mods];
	memset(mods, 0, num_mods * sizeof(unsigned int));

	for (int i = 1; i < argc; ++i) {
		uint64_t result = hash64((unsigned char const *) argv[i],
		                         strlen(argv[i]));

		for (int j = 3; j < num_mods; ++j) {
			if (result % j == 0) {
				++(mods[j - 3]);
			}
		}

		results[i - 1] = result;
	}

	unsigned int worst_count = INT_MAX;

	for (int i = 0; i < argc - 1; ++i) {
		for (int j = 0; j < argc - 1; ++j) {
			if (i == j) {
				continue;
			}

			unsigned int count = popcount(results[i] ^ results[j]);
			if (count < worst_count) {
				worst_count = count;
			}
		}
	}

	bool first = true;

	for (int i = 0; i < num_mods; ++i) {
		if (mods[i] == argc - 1) {
			if (first) {
				printf("Common factors:\n");
				first = false;
			}

			printf("%d\n", i + 3);
		}
	}

	if (first) {
		printf("No common factors\n");
	}

	printf("\nWorst case bit difference: %u (%.2f%%)\n", worst_count,
	       worst_count / (sizeof(uint64_t) * 8.0) * 100.0);

	free(results);

	return 0;
}
