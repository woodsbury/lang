all : hashdiff

hashdiff : build/tools/hashdiff/hashdiff.o build/source/common/hash.o
	$(CC) $(CFLAGS) $(LDFLAGS) $^ $(LOADLIBES) -o $@
